# Dialog 对话框

### 基本使用

<ClientOnly>
<template>
  <div class="dialog-container">
    <nj-button type="primary" @click="visible = true">对话框</nj-button>
    <nj-dialog title="温馨提示" :visible.sync="visible">
      <template v-slot:footer>
        <nj-button @click="visible = false">取消</nj-button>
        <nj-button type="warning">确定</nj-button>
      </template>
    </nj-dialog>
  </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
    <nj-button type="primary" @click="visible = true">对话框</nj-button>
    <nj-dialog title="温馨提示" :visible.sync="visible">
      <template v-slot:footer>
        <nj-button @click="visible = false">取消</nj-button>
        <nj-button type="warning">确定</nj-button>
      </template>
    </nj-dialog>
```
:::

<script>
export default {
  data() {
    return {
      visible: false
    };
  }
}
</script>

<style scoped>
.dialog-container {
  width: 700px;
  height: 80px;
  display: flex;
  justify-content: center;
  align-items: center;
}
</style>
