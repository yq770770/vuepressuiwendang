# Radio 单选框

### 基本使用

<ClientOnly>
<template>
    <div class="radio-container">
      <nj-radio label="0" v-model="gender"></nj-radio>
      <nj-radio label="1" v-model="gender"></nj-radio>
    </div>
    <div class="radio-container">
      <nj-radio label="0" v-model="gender">男</nj-radio>
      <nj-radio label="1" v-model="gender">女</nj-radio>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-radio label="0" v-model="gender"></nj-radio>
  <nj-radio label="1" v-model="gender"></nj-radio>
  <nj-radio label="0" v-model="gender">男</nj-radio>
  <nj-radio label="1" v-model="gender">女</nj-radio>
```
:::

### radio-group

<ClientOnly>
<template>
    <div class="radio-container">
      <nj-radio-group v-model="game">
        <nj-radio label="0">英雄联盟</nj-radio>
        <nj-radio label="1">Pubg</nj-radio>
        <nj-radio label="2">H1Z1</nj-radio>
      </nj-radio-group>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-radio-group v-model="game">
    <nj-radio label="0">英雄联盟</nj-radio>
    <nj-radio label="1">Pubg</nj-radio>
    <nj-radio label="2">H1Z1</nj-radio>
  </nj-radio-group>  
```
:::

<script>
export default {
  data(){
    return{
      gender: "0",
      game: "0",
    }
  }
}
</script>
<style scoped>
.radio-container {
  width: 700px;
  height: 80px;
  display: flex;
  justify-content: space-around;
  align-items: center;
}
</style>
