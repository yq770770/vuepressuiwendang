# Input 输入框

### 基本用法

<ClientOnly>
<template>
    <div class="input-container">
      <nj-input placeholder="请输入内容" type="text"></nj-input>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-input placeholder="请输入内容" type="text"></nj-input>
```

:::
### 绑定事件

<ClientOnly>
<template>
    <div class="input-container">
      <nj-input placeholder="请输入内容" type="text" v-model="username"></nj-input>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
      <nj-input placeholder="请输入内容" type="text" v-model="username"></nj-input>
```
:::

### 清空内容
<ClientOnly>
<template>
    <div class="input-container">
      <nj-input placeholder="请输入用户名" type="text" v-model="username" clearable></nj-input>
    </div>
</template>
</ClientOnly>
::: details 点击查看代码
```vue
  <nj-input placeholder="请输入用户名" type="text" v-model="username" clearable></nj-input>
```
:::

### 显示密码
<ClientOnly>
<template>
    <div class="input-container">
      <nj-input placeholder="请输入密码" type="password" v-model="password" showPwd></nj-input>
    </div>
</template>
</ClientOnly>
::: details 点击查看代码
```vue
  <nj-input placeholder="请输入密码" type="password" v-model="password" showPwd></nj-input>
```
:::

### 禁用
<ClientOnly>
<template>
    <div class="input-container">
      <nj-input placeholder="请输入内容" type="password" name="password" disabled></nj-input>
    </div>
</template>
</ClientOnly>
::: details 点击查看代码
```vue
  <nj-input placeholder="请输入内容" type="password" name="password" disabled></nj-input>
```

<script>
export default {
  data() {
    return {
      username:'',
      password: ''
    };
  }
}
</script>

<style scoped>
.input-container {
  width: 700px;
  height: 80px;
  display: flex;
  justify-content: space-around;
  align-items: center;
}
</style>