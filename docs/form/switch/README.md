# Switch 开关

### 基本用法

<ClientOnly>
<template>
    <div class="switch-container">
      <nj-switch v-model="active"></nj-switch>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-switch v-model="active"></nj-switch>
```
:::

### 颜色

<ClientOnly>
<template>
  <div class="switch-container">
      <nj-switch v-model="active" activeColor="red" inactiveColor="blue"></nj-switch>
      <nj-switch v-model="active" activeColor="white" inactiveColor="black" name="username"></nj-switch>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-switch v-model="active" activeColor="red" inactiveColor="blue"></nj-switch>
  <nj-switch v-model="active" 
  activeColor="white" 
  inactiveColor="black" 
  name="username"></nj-switch>
```
:::

<script>
export default {
  data() {
    return {
      active: false
    };
  }
}
</script>

<style scoped>
.switch-container {
  width: 700px;
  height: 80px;
  display: flex;
  justify-content: space-around;
  align-items: center;
}
</style>

