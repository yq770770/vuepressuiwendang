# Checkbox 复选框

### 基本用法

<ClientOnly>
<template>
    <div class="check-container">
      <nj-checkbox v-model="checked">是否选中</nj-checkbox>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-checkbox v-model="checked">是否选中</nj-checkbox>
```
:::

### checkbox-group

<ClientOnly>
<template>
    <div class="check-container">
      <nj-checkbox-group v-model="selectAns">
        <nj-checkbox label="A"></nj-checkbox>
        <nj-checkbox label="B"></nj-checkbox>
        <nj-checkbox label="C"></nj-checkbox>
      </nj-checkbox-group>
    </div>
</template>
</ClientOnly>

::: details 点击查看代码
```vue
  <nj-checkbox-group v-model="selectAns">
    <nj-checkbox label="A"></nj-checkbox>
    <nj-checkbox label="B"></nj-checkbox>
    <nj-checkbox label="C"></nj-checkbox>
  </nj-checkbox-group>
```
:::

<script>
export default {
  data(){
    return{
      checked: false,
      selectAns: []
    }
  }
}
</script>

<style scoped>
.check-container {
  width: 700px;
  height: 80px;
  display: flex;
  justify-content: space-around;
  align-items: center;
}
</style>