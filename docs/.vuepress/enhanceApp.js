import 'ninja-ui-ts/dist/ninja-ui-ts.css'
import NinJaUI from "ninja-ui-ts";
export default ({ Vue, options, router, siteData }) => {
  Vue.use(NinJaUI);
};
