module.exports = {
  base: "/vuepressuiwendang/",
  title: "NinJa-UI",
  head: [["link", { rel: "shortcut icon", href: "/favicon.ico" }]],
  markdown: {
    lineNumbers: true,
  },
  themeConfig: {
    logo: "/logo.jpg",
    nav: [
      { text: "Home", link: "/" },
      { text: "Guide", link: "/guide/" },
      {
        text: "About me",
        items: [
          { text: "My Blog", link: "https://yq427.cn" },
          { text: "Gitee", link: "https://gitee.com/yq770770" },
          { text: "Github", link: "https://github.com/731391599" },
          { text: "Npm", link: "https://www.npmjs.com/~ninjayq" },
        ],
      },
    ],
    sidebar: [
      {
        title: "安装",
        path: "/guide/",
        collapsable: false,
      },
      {
        title: "Basic",
        path: "/basic/",
        collapsable: false,
        children: ["/basic/button/"],
      },
      {
        title: "Form",
        path: "/form/",
        collapsable: false,
        children: [
          "/form/input/",
          "/form/switch/",
          "/form/radio/",
          "/form/checkbox/",
          "/form/form/",
        ],
      },
      {
        title: "Data",
        path: "/data/",
        collapsable: false,
        children: ["/data/tag/"],
      },
      {
        title: "Notice",
        path: "/notice/",
        collapsable: false,
      },
      {
        title: "Others",
        path: "/others/",
        collapsable: false,
        children: ["/others/dialog/"],
      },
    ],
  },
};
